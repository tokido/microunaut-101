Installation instructions :

App : [http://localhost:3000/](http://localhost:3000/).

StoreFront & API : [http://localhost:8080/](http://localhost:8080/).

Consul : [http://localhost:8500/](http://localhost:8500/).

MongoDB : [http://localhost:27017/](http://localhost:27017/).

Neo4j : [http://localhost:7687/](http://localhost:7687/).

## Etapes de démarrage :

docker-compose up consul mongodb neo4j

./gradlew movies:run

./gradlew members:run

./gradlew comments:run

./gradlew storefront:run

./gradlew frontend:react:start (avec browsersync)

## Demarrage avec docker

./gradlew build -x test (skipper le test parce que ça ne passe pas actuellement)

docker-compose build

docker-compose up

## Architecture

Front (React)
	|
	|
Store Front (Groovy)
	| 	 	\
 	| 		 \
API (Java)   Consul (Service Registry)
