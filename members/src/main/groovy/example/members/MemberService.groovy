package example.members

import grails.gorm.services.Service
import javax.validation.constraints.NotBlank

@Service(Member)
abstract class MemberService {
    /**
     * List all of the members
     *
     * @return The members
     */
    abstract List<Member> list()

    /**
     * @return list the member names
     */
    abstract List<String> listMemberName()

    /**
     * Save a new member
     * @param name The name of the member
     * @return The member instance
     */
    abstract Member save(@NotBlank String name)

    /**
     * Finds a new member
     * @param name The name of the member
     * @return The member instance
     */
    abstract Member find(@NotBlank String name)

    /**
     * Find an existing member or create a new one
     * @param name The name of the member
     * @return The Member
     */
    Member findOrCreate(@NotBlank String name) {
        Member v = find(name)
        if(v == null) {
            v = save(name)
        }
        return v
    }
}