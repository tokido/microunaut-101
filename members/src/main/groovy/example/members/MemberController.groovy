package example.members

import example.api.v1.Movie
import example.members.client.v1.MovieClient
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated
import javax.inject.Singleton
import javax.validation.constraints.NotBlank

@Controller('/${members.api.version}/members')
@Validated
class MemberController {

    final MemberService memberService
    final MovieClient movieClient

    MemberController(MemberService memberService, MovieClient movieClient) {
        this.memberService = memberService
        this.movieClient = movieClient
    }

    @Get('/')
    Single<List<Member>> list() {
        return Single.fromCallable({-> memberService.list() })
              .subscribeOn(Schedulers.io())
              .toFlowable()
              .flatMap({ List<Member> list ->
            Flowable.fromIterable(list)
        })
        .flatMap({ Member v ->
            movieClient.byMember(v.name).map({ List<Movie> movies ->
                return v.movies(movies)
            }).toFlowable()
        })
        .toList()

    }

    @Get('/names')
    List<String> names() {
        memberService.listMemberName()
    }

    @Post('/')
    Member save(@NotBlank String name) {
        memberService.findOrCreate(name)
    }
}
