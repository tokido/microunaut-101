package example.members

import example.api.v1.Movie
import grails.gorm.annotation.Entity

@Entity
class Member extends example.api.v1.Member {

    @Override
    void setName(String name) {
        super.setName(name)
    }

    Member movies(List<Movie> movies) {
        setMovies(movies)
        return this
    }
}
