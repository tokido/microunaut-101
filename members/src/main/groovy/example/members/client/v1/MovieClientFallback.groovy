package example.members.client.v1

import example.api.v1.Movie
import example.api.v1.MovieOperations
import io.reactivex.Maybe
import io.reactivex.Single
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.retry.annotation.Fallback

import javax.inject.Singleton
import javax.validation.Valid

@Fallback
@Singleton
class MovieClientFallback implements MovieOperations<Movie>{
    @Override
    Single<List<Movie>> list() {
        return Single.just([])
    }

    @Override
    Single<List<Movie>> byMember(String name) {
        return Single.just([])
    }

    @Override
    Maybe<Movie> random() {
        return Maybe.empty()
    }

    @Override
    Maybe<Movie> find(String slug) {
        return Maybe.empty()
    }

    @Override
    Single<Movie> save(@Valid @Body Movie movie) {
        return Single.error(new RuntimeException("Cannot save movies at the moment. No available servers."))
    }
}
