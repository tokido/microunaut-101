package example.members.client.v1

import example.api.v1.Movie
import example.api.v1.MovieOperations
import io.reactivex.Maybe
import io.reactivex.Single
import io.micronaut.http.client.annotation.Client

@Client(id = 'movies', path = "/v1/movies")
interface MovieClient extends MovieOperations<Movie> {

    @Override
    Single<List<Movie>> byMember(String name)

    @Override
    Maybe<Movie> find(String slug)
}