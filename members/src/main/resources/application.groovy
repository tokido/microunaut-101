micronaut.application.name="members"
micronaut.server.port=-1
consul.client.defaultZone='${CONSUL_HOST:localhost}:${CONSUL_PORT:8500}'
members.api.version="v1"
hibernate {
    hbm2ddl {
        auto = "create-drop"
    }
}