package example.members

import example.api.v1.Member
import io.reactivex.Single
import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class MemberControllerSpec extends Specification {

    @Shared @AutoCleanup EmbeddedServer embeddedServer =
            ApplicationContext.run(EmbeddedServer, Collections.singletonMap(
                    "consul.client.registration.enabled",false
            ))

    @Shared Member memberOperations = embeddedServer
                                                    .getApplicationContext()
                                                    .getBean(Member)

    void 'test list members'() {
        when:
        List<example.api.v1.Member> members = memberOperations.list().blockingGet()

        then:
        members.size() == 0
    }

    void 'test save member'() {
        when:
        example.api.v1.Member v = memberOperations.save("Nexus").blockingGet()

        then:
        v != null
        v.name == "Nexus"
        memberOperations.list().blockingGet().size() == 1
        memberOperations.names().blockingGet().contains("Nexus")

    }

    @Client('/${members.api.version}/members')
    static interface TestMember extends MemberOperations {
        @Override
        Single<example.api.v1.Member> save(String name)
    }
}
