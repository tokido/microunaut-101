import React from "react";
import { Link } from "react-router-dom";
import config from "../config";
import { useFetchState } from "../fetch-util";

export default function FeaturedMovie() {
  const [movie] = useFetchState("/movies/random");

  if (!movie) return null;

  return (
    <div className="card featured-card">
      <Link to={`/movies/${movie.slug}`}>
        <img
          className="card-img-top"
          src={`${config.SERVER_URL}/images/${movie.image}`}
          alt={movie.name}
        />
      </Link>
      <div className="card-body">
        <h5 className="card-title">{movie.name}</h5>
        <Link to={`/movies/${movie.slug}`} className="btn btn-primary">
          More info
        </Link>
      </div>
    </div>
  );
}
