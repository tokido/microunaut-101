import React, { useEffect, useRef, useState } from "react";
import FeaturedMovie from "./FeaturedMovie";
import config from "../config";
import Alert from "../display/Alert";

export default function Home() {
  const [error, setError] = useState();
  const source = useRef();

  useEffect(() => {
    source.current = new EventSource(`${config.SERVER_URL}/movies/random`);
    source.current.onerror = () => {
      setError("Could not load featured movies");
    };
    return () => source.current.close();
  }, []);

  return (
    <div>
      <Alert message={error} level="warning" />

      <h2>Popular Movies</h2>
      <div className="row">
        <div className="col-md-6">
          <FeaturedMovie />
        </div>
        <div className="col-md-6">
          <FeaturedMovie />
        </div>
      </div>
    </div>
  );
}
