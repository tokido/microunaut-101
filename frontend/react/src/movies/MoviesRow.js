import { array } from "prop-types";
import React from "react";
import MovieCell from "./MovieCell";

const MoviesRow = ({ movies }) => (
  <div className="row">
    {movies.map((movie, index) => (
      <MovieCell key={index} movie={movie} />
    ))}
  </div>
);

MoviesRow.propTypes = { movies: array };

export default MoviesRow;
