import {shape, string} from 'prop-types';
import React from 'react';
import {Link} from 'react-router-dom';
import config from '../config';

const getStyle = movie => ({
  backgroundImage: `url(${config.SERVER_URL}/images/${movie.image})`
});

const MovieCell = ({movie}) => (
  <div className="col-sm movie-card" style={getStyle(movie)}>
    <Link className="movie-link" to={`/movies/${movie.slug}`}>
      <div className="movie-header">
        <h4>{movie.name}</h4>
      </div>
    </Link>
  </div>
);

MovieCell.propTypes = {
  movie: shape({
    image: string,
    name: string,
    slug: string
  })
};

export default MovieCell;
