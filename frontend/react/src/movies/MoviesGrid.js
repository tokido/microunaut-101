import { array } from "prop-types";
import React from "react";
import MoviesRow from "./MoviesRow";

function MoviesGrid({ movies }) {
  const groupByThree = (array, movie, index) => {
    const rowIndex = Math.floor(index / 3);
    if (!array[rowIndex]) array[rowIndex] = [];
    array[rowIndex].push(movie);
    return array;
  };

  const rows = movies.reduce(groupByThree, []);

  return (
    <div>
      {rows.map((row, index) => (
        <MoviesRow key={index} movies={row} />
      ))}
    </div>
  );
}

MoviesGrid.propTypes = {
  movies: array
};

export default MoviesGrid;
