import { shape, string } from "prop-types";
import React from "react";
import MoviesLayout from "./MoviesLayout";
import { useFetchState } from "../fetch-util";

function MemberMovies({ match }) {
  const { member } = match.params;
  const [movies] = useFetchState(`/movies/member/${member}`, []);

  return (
    <MoviesLayout
      header={`Movies proposed by ${member}`}
      match={match}
      movies={movies}
    />
  );
}

MemberMovies.propTypes = {
  match: shape({
    params: shape({
      member: string
    })
  })
};

export default MemberMovies;
