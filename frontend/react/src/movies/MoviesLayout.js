/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { array, shape, string } from "prop-types";
import React, { useState } from "react";
import { Route } from "react-router-dom";
import MoviesGrid from "./MoviesGrid";

function moviesForTab(index, movies) {
  const type = index === 1 ? "SCIFI" : index === 2 ? "HORROR" : null;
  return type ? movies.filter(p => p.type === type) : movies;
}

function MoviesLayout({ header, match, movies }) {
  const [tab, setTab] = useState(1);

  const render = () => (
    <div>
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">{header || "Movies"}</h1>
        </div>
      </div>

      <ul className="nav nav-tabs">
        <li className="nav-item">
          <span
            className={`nav-link ${tab === 1 ? "active" : ""}`}
            onClick={() => setTab(1)}
          >
            SCIFI
          </span>
        </li>
        <li className="nav-item">
          <span
            className={`nav-link ${tab === 2 ? "active" : ""}`}
            onClick={() => setTab(2)}
          >
            HORROR
          </span>
        </li>
      </ul>

      <MoviesGrid movies={moviesForTab(tab, movies)} />
    </div>
  );

  return <Route exact path={match.url} render={render} />;
}

MoviesLayout.propTypes = {
  header: string,
  match: shape({
    url: string
  }),
  movies: array
};

export default MoviesLayout;
