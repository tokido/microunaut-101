import { shape, string } from "prop-types";
import React from "react";
import MoviesLayout from "./MoviesLayout";
import { useFetchState } from "../fetch-util";

function Movies({ match }) {
  const [movies] = useFetchState("/movies", []);
  return <MoviesLayout movies={movies} match={match} />;
}

Movies.propTypes = {
  match: shape({
    params: shape({
      slug: string
    })
  })
};

export default Movies;
