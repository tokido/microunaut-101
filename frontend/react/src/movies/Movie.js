import { shape, string } from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import Comments from "../comments/Comments";
import config from "../config";
import { useFetchState } from "../fetch-util";

function Movie({ match }) {
  const [movie] = useFetchState(`/movies/${match.params.slug}`);

  if (!movie) return <span>Loading...</span>;

  return (
    <div className="row">
      <div className="col-md-6">
        <h1>{movie.name}</h1>
        <h4>Member: {movie.member}</h4>
        <p>
          <Link
            to={`/movies/member/${movie.member}`}
            className="btn btn-primary"
          >
            More Movies from {movie.member}
          </Link>
        </p>

        <Comments topic={movie.slug} />
      </div>
      <div className="col-md-6">
        <img
          style={{ maxWidth: "70%" }}
          src={`${config.SERVER_URL}/images/${movie.image}`}
          alt={movie.name}
        />
      </div>
    </div>
  );
}

Movie.propTypes = {
  match: shape({
    params: shape({
      slug: string
    })
  })
};

export default Movie;
