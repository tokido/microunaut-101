import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import About from "./about/About";
import Home from "./home/Home";
import Movie from "./movies/Movie";
import Movies from "./movies/Movies";
import MemberMovies from "./movies/MemberMovies";
import Members from "./members/Members";

import logo from "./images/logo.png";
import "./App.css";

const App = () => (
  <Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand">
          <img src={logo} className="micronaut-logo" alt="micronaut" /> Example
          MovieReview
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/movies" className="nav-link">
                Movies
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/members" className="nav-link">
                Members
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/about" className="nav-link">
                About
              </Link>
            </li>
          </ul>
        </div>
      </nav>

      <div className="container">
        <Route exact path="/" component={Home} />
        <Route exact path="/movies" component={Movies} />
        <Route exact path="/movies/:slug" component={Movie} />
        <Route exact path="/movies/member/:member" component={MemberMovies} />
        <Route exact path="/members" component={Members} />
        <Route exact path="/about" component={About} />
      </div>
    </div>
  </Router>
);

export default App;
