import React from "react";
import MembersTable from "./MembersTable";
import { useFetchState } from "../fetch-util";

export default function Members() {
  const [members] = useFetchState("/members", []);

  return (
    <div>
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Members</h1>
        </div>
      </div>
      <MembersTable members={members} />
    </div>
  );
}
