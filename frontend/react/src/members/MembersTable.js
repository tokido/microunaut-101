import { array } from "prop-types";
import React from "react";
import MemberCard from "./MemberCard";

const MembersTable = ({ members }) => (
  <div>
    {members.map((member, index) => (
      <MemberCard key={index} member={member} />
    ))}
  </div>
);

MembersTable.propTypes = {
  members: array.isRequired
};

export default MembersTable;
