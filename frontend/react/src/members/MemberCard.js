import { shape, array, string } from "prop-types";
import React from "react";
import { Link } from "react-router-dom";
import config from "../config";

function MemberCard({ member }) {
  const imgSrc =
    member.movies && member.movies.length > 0
      ? member.movies[0].image
      : "missing.png";
  const imgUrl = `${config.SERVER_URL}/images/${imgSrc}`;
  const linkTo = `/movies/member/${member.name}`;

  return (
    <div className="card member-card">
      <Link to={linkTo}>
        <img alt={member.name} className="card-img-top" src={imgUrl} />
      </Link>
      <div className="card-body">
        <h5 className="card-title">{member.name}</h5>
        <p className="card-text">
          Movies: {member.movies ? member.movies.length : 0}
        </p>
        <Link to={linkTo} className="btn btn-primary">
          See all Movies
        </Link>
      </div>
    </div>
  );
}

MemberCard.propTypes = {
  member: shape({
    name: string,
    movies: array
  })
};

export default MemberCard;
