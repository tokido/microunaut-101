package example.movies;

import example.api.v1.MovieOperations;
import io.micronaut.http.client.annotation.Client;
import io.reactivex.Single;
import java.util.List;

@Client("/${movies.api.version}/movies")
interface MovieControllerTestClient extends MovieOperations<MovieEntity> {
    @Override
    Single<List<MovieEntity>> byMember(String name);
}
