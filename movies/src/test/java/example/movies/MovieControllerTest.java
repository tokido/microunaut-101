package example.movies;

import com.mongodb.reactivestreams.client.MongoClient;
import example.api.v1.Movie;
import example.api.v1.MovieType;
import io.reactivex.Flowable;
import org.junit.*;
import io.micronaut.configuration.mongo.reactive.MongoSettings;
import io.micronaut.context.ApplicationContext;
import io.micronaut.core.io.socket.SocketUtils;
import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.runners.MethodSorters;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MovieControllerTest {

    static EmbeddedServer embeddedServer;

    @BeforeClass
    public static void setup() {
        embeddedServer = ApplicationContext.run(EmbeddedServer.class,
                CollectionUtils.mapOf(MongoSettings.MONGODB_URI,
                        "mongodb://localhost:" + SocketUtils.findAvailableTcpPort(),
                        "consul.client.registration.enabled", false));
    }

    @AfterClass
    public static void cleanup() {
        if (embeddedServer != null) {
            embeddedServer.stop();
        }
    }

    @After
    public void cleanupData() {
        ApplicationContext applicationContext = embeddedServer.getApplicationContext();
        MongoClient mongoClient = applicationContext.getBean(MongoClient.class);
        MoviesConfiguration config = applicationContext.getBean(MoviesConfiguration.class);
        // drop the data
        Flowable.fromPublisher(mongoClient.getDatabase(config.getDatabaseName()).drop()).blockingFirst();
    }

    @Test
    public void testListMovies() {
        MovieControllerTestClient client = embeddedServer.getApplicationContext()
                .getBean(MovieControllerTestClient.class);

        List<MovieEntity> movies = client.list().blockingGet();
        assertEquals(0, movies.size());

        try {
            client.save(new MovieEntity("", "", "")).blockingGet();
            fail("Should have thrown a constraint violation");
        } catch (HttpClientResponseException e) {
            assertEquals(e.getStatus(), HttpStatus.BAD_REQUEST);
        }

        MovieEntity entity = new MovieEntity("Hoho", "Haha", "photo-1457914109735-ce8aba3b7a79.jpeg")
                .type(MovieType.HORROR);
        Movie harry = client.save(entity).blockingGet();

        assertNotNull(harry);

        assertEquals(harry.getImage(), entity.getImage());
        assertEquals(harry.getName(), entity.getName());
        assertEquals(harry.getType(), entity.getType());

        movies = client.list().blockingGet();
        assertEquals(movies.size(), 1);
        assertEquals(movies.iterator().next().getName(), harry.getName());

    }

    @Test
    public void testNextFindByMember() {
        MovieControllerTestClient client = embeddedServer.getApplicationContext()
                .getBean(MovieControllerTestClient.class);

        MovieEntity entity = new MovieEntity("Hoho", "Hihi", "photo-1442605527737-ed62b867591f.jpeg")
                .type(MovieType.SCIFI);

        Movie ron = client.save(entity).blockingGet();

        assertNotNull(ron);

        assertEquals(1, client.byMember("Hoho").blockingGet().size());
    }
}
