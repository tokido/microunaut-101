package example.movies;

import java.text.Normalizer;

public class FriendlyUrl {

    /**
     *
     * http://core.svn.wordpress.org/trunk/wp-includes/formatting.php
     */
    public static String sanitizeWithDashes(String text) {

        if (text == null) {
            return "";
        }

        // Preserve escaped octets
        text = text.replaceAll("%([a-fA-F0-9][a-fA-F0-9])", "---$1---");
        text = text.replaceAll("%", "");
        text = text.replaceAll("---([a-fA-F0-9][a-fA-F0-9])---", "%$1");

        // Remove accents
        text = removeAccents(text);

        // To lower case
        text = text.toLowerCase();

        // Kill entities
        text = text.replaceAll("&.+?;", "");

        // Dots -> ''
        text = text.replaceAll("\\.", "");

        // Remove any character except %a-zA-Z0-9 _-
        text = text.replaceAll("[^%a-zA-Z0-9 _-]", "");

        // Trim
        text = text.trim();

        // Spaces -> dashes
        text = text.replaceAll("\\s+", "-");

        // Dashes -> dash
        text = text.replaceAll("-+", "-");

        if (!Character.isLetterOrDigit(text.substring(text.length() - 1).charAt(0))) {
            text = text.substring(0, text.length() - 1);
        }

        return text;
    }

    /**
     * Converts all accent characters to ASCII characters.
     */
    private static String removeAccents(String text) {
        return Normalizer.normalize(text, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
