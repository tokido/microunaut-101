package example.movies;

import com.mongodb.client.model.Aggregates;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import example.api.v1.MovieOperations;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.micronaut.configuration.hystrix.annotation.HystrixCommand;
import io.micronaut.http.annotation.Controller;
import io.micronaut.validation.Validated;

import javax.inject.Singleton;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

@Controller("/${movies.api.version}/movies")
@Validated
public class MovieController implements MovieOperations<MovieEntity> {

    private final MoviesConfiguration configuration;
    private MongoClient mongoClient;

    public MovieController(MoviesConfiguration configuration, MongoClient mongoClient) {
        this.configuration = configuration;
        this.mongoClient = mongoClient;
    }

    @Override
    @HystrixCommand
    public Single<List<MovieEntity>> list() {
        return Flowable.fromPublisher(getCollection().find()).toList();
    }

    @Override
    @HystrixCommand
    public Maybe<MovieEntity> random() {
        // return Flowable.fromPublisher(getCollection().find(eq("slug",
        // "star-wars")).limit(1)).firstElement();

        return Flowable
                .fromPublisher(
                        getCollection().aggregate(Collections.singletonList(Aggregates.sample(1)), MovieEntity.class))
                .firstElement();
    }

    @Override
    public Single<List<MovieEntity>> byMember(String name) {
        return Flowable.fromPublisher(getCollection().find(eq("member", name))).toList();
    }

    @Override
    public Maybe<MovieEntity> find(String slug) {
        return Flowable.fromPublisher(getCollection().find(eq("slug", slug)).limit(1)).firstElement();
    }

    @Override
    public Single<MovieEntity> save(@Valid MovieEntity movie) {
        String slug = FriendlyUrl.sanitizeWithDashes(movie.getName());
        movie.slug(slug);
        return find(slug).switchIfEmpty(Single.fromPublisher(getCollection().insertOne(movie)).map(success -> movie));
    }

    private MongoCollection<MovieEntity> getCollection() {
        return mongoClient.getDatabase(configuration.getDatabaseName()).getCollection(configuration.getCollectionName(),
                MovieEntity.class);
    }
}
