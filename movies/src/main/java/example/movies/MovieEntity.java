package example.movies;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import example.api.v1.MovieType;
import org.bson.codecs.pojo.annotations.BsonCreator;
import org.bson.codecs.pojo.annotations.BsonProperty;
import example.api.v1.Movie;


public class MovieEntity extends Movie {
    @BsonCreator
    @JsonCreator
    public MovieEntity(
            @JsonProperty("member")
            @BsonProperty("member") String member,
            @JsonProperty("name")
            @BsonProperty("name") String name,
            @JsonProperty("image")
            @BsonProperty("image") String image) {
        super(member, name, image);
    }

    @Override
    public MovieEntity type(MovieType type) {
        return (MovieEntity) super.type(type);
    }

    @Override
    public MovieEntity slug(String slug) {
        return (MovieEntity) super.slug(slug);
    }

    @Override
    public void setSlug(String image) {
        super.setSlug(image);
    }

    @Override
    public void setImage(String image) {
        super.setImage(image);
    }

    @Override
    public void setType(MovieType type) {
        super.setType(type);
    }
}
