package example.movies;

import io.micronaut.context.annotation.ConfigurationProperties;

@ConfigurationProperties("movies")
public class MoviesConfiguration {
    private String databaseName = "moviestore";
    private String collectionName = "movietests";

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}
