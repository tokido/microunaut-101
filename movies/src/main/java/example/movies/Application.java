package example.movies;

import io.micronaut.runtime.Micronaut;

import javax.inject.Singleton;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;
import io.swagger.v3.oas.annotations.tags.*;

@OpenAPIDefinition(info = @Info(title = "Movies", version = "0.0", description = "Emulation Movie DB", license = @License(name = "Apache 2.0", url = "http://moviestore.notreal")), tags = {
        @Tag(name = "Movie"), @Tag(name = "Anime") })
@Singleton
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}
