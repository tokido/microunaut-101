package example.comments

import example.api.v1.CommentOperations
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.annotation.Client

import javax.validation.constraints.NotBlank

@Client('/${comments.api.version}/topics')
interface TestCommentClient extends CommentOperations<Comment>{

    @Override
    HttpStatus addReply(@NotBlank Long id, @NotBlank String poster, @NotBlank String content) 

    @Override
    Map<String, Object> expand(Long id) 

    @Override
    List<Comment> list(String topic)

    @Override
    HttpStatus add(@NotBlank String topic, @NotBlank String poster, @NotBlank String content)
}
