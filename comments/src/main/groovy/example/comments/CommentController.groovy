package example.comments

import example.api.v1.CommentOperations
import groovy.transform.CompileStatic
import io.micronaut.http.HttpStatus
import io.micronaut.http.annotation.Controller

import javax.validation.constraints.NotBlank

@Controller('/${comments.api.version}/topics')
@CompileStatic
class CommentController implements CommentOperations<Comment> {

    final CommentRepository commentRepository

    CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository
    }

    @Override
    HttpStatus add(
            @NotBlank String topic,
            @NotBlank String poster,
            @NotBlank String content) {
        Comment c = commentRepository.saveComment(
                topic, poster, content
        )
        if(c != null) {
            return HttpStatus.CREATED
        }
        return HttpStatus.NOT_FOUND
    }

    @Override
    HttpStatus addReply(
            @NotBlank Long id,
            @NotBlank String poster,
            @NotBlank String content) {
        Comment c = commentRepository.saveReply(
                id, poster, content
        )
        if(c != null) {
            return HttpStatus.CREATED
        }
        return HttpStatus.NOT_FOUND
    }

    @Override
    List<Comment> list(String topic) {
        return commentRepository.findComments(topic)
    }

    @Override
    Map<String, Object> expand(Long id) {
        return commentRepository.findCommentReplies(id)
    }
}
