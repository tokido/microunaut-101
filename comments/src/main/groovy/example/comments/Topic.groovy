package example.comments

import grails.gorm.annotation.Entity
import grails.neo4j.Neo4jEntity
import grails.neo4j.Node
import grails.neo4j.mapping.MappingBuilder

@Entity
class Topic implements Node<Topic> {
    
    String title
    List<Comment> comments
    static hasMany = [comments:Comment]

    static mapping = MappingBuilder.node {
        id generator:'assigned', name:'title'
        version false
    }
}
