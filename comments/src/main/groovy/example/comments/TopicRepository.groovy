package example.comments

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import grails.neo4j.services.Cypher
import groovy.transform.CompileStatic

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@Service(Topic)
@CompileStatic
interface TopicRepository {

    /**
     * Finds a topic for the given title
     * @param title The title
     * @return The topic
     */
    Topic findTopic(@NotBlank String title)

    /**
     * Finds a topic for the given title
     * @param title The title
     * @return The topic
     */
    Topic saveTopic(@NotBlank String title)

}