package example.comments

import com.fasterxml.jackson.annotation.JsonIgnore
import grails.gorm.annotation.Entity
import grails.neo4j.Neo4jEntity
import grails.neo4j.Node
import groovy.transform.ToString

import javax.validation.constraints.NotBlank

@Entity
@ToString(includes= ['id','poster', 'content'])
class Comment implements Node<Comment>, example.api.v1.Comment {
    Long id
    @NotBlank
    String poster
    @NotBlank
    String content
    Date dateCreated
    
    @JsonIgnore
    List<Comment> replies = []
    static hasMany = [replies: Comment]
}
