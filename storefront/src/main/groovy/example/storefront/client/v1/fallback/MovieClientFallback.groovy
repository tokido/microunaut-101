package example.storefront.client.v1.fallback

import example.api.v1.Movie
import example.api.v1.MovieOperations
import groovy.transform.CompileStatic
import io.reactivex.Maybe
import io.reactivex.Single
import io.micronaut.http.annotation.Body
import io.micronaut.retry.annotation.Fallback

import javax.validation.Valid

@Fallback
@CompileStatic
class MovieClientFallback implements MovieOperations<Movie> {
    @Override
    Single<List<Movie>> list() {
        return Single.just([] as List<Movie>)
    }

    @Override
    Single<List<Movie>> byMember(String name) {
        return list()
    }

    @Override
    Maybe<Movie> random() {
        return Maybe.empty()
    }

    @Override
    Maybe<Movie> find(String slug) {
        return Maybe.empty()
    }

    @Override
    Single<Movie> save(@Valid @Body Movie movie) {
        return Single.just(movie)
    }
}
