package example.storefront.client.v1

import example.api.v1.Member
import example.api.v1.MemberOperations
import io.reactivex.Single
import io.micronaut.http.client.annotation.Client

@Client(id = "members", path = "/v1/members")
interface MemberClient extends MemberOperations{
    @Override
    Single<Member> save(String name)
}