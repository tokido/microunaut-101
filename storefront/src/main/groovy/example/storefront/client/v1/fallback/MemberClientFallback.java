package example.storefront.client.v1.fallback;

import example.api.v1.Member;
import example.api.v1.MemberOperations;
import groovy.transform.CompileStatic;
import io.reactivex.Single;
import io.micronaut.retry.annotation.Fallback;

import java.util.Collections;
import java.util.List;

@Fallback
@CompileStatic
public class MemberClientFallback implements MemberOperations {
    @Override
    public Single<List<Member>> list() {
        return Single.just(Collections.emptyList());
    }

    @Override
    public Single<List<String>> names() {
        return Single.just(Collections.emptyList());
    }

    @Override
    public Single<Member> save(String name) {
        return Single.just(new Member(name));
    }
}
