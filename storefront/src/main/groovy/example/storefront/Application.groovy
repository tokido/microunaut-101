package example.storefront

import example.api.v1.Movie
import example.api.v1.MovieType
import example.api.v1.Member
import example.storefront.client.v1.MovieClient
import example.storefront.client.v1.MemberClient
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.micronaut.runtime.Micronaut
import io.micronaut.runtime.event.annotation.EventListener
import io.reactivex.Flowable
import io.reactivex.Single
import io.micronaut.runtime.server.event.ServerStartupEvent

import javax.inject.Singleton

@Singleton
@Slf4j
@CompileStatic
class Application {

    final MovieClient movieClient
    final MemberClient memberClient

    Application(MovieClient movieClient, MemberClient memberClient) {
        this.movieClient = movieClient
        this.memberClient = memberClient
    }

    @EventListener
    void onStartup(ServerStartupEvent event) {
        def names = ["InThePanda", "Nexus", "Fossoyeur"]
        List<Flowable<Member>> saves = []
        for (name in names) {
            /**
            * @TODO here you must update on lunch
            */
            saves.add(memberClient.save(name).toFlowable().flatMap({ Member member ->
                List<Single<Movie>> operations = []
                String memberName = member.name
                if (memberName == 'InThePanda') {
                    operations.add(movieClient.save(new Movie(memberName, "Ready Player One", "rpl.jpg").type(MovieType.SCIFI)))
                    operations.add(movieClient.save(new Movie(memberName, "Avatar", "avatar.jpg").type(MovieType.SCIFI)))
                    operations.add(movieClient.save(new Movie(memberName, "Hereditary", "hereditary.jpg").type(MovieType.HORROR)))
                } else if (memberName == 'Nexus') {
                    operations.add(movieClient.save(new Movie(memberName, "Star Wars", "star-wars.jfif").type(MovieType.SCIFI)))
                    operations.add(movieClient.save(new Movie(memberName, "Visitor Q", "visitorq.jpg").type(MovieType.HORROR)))
                    operations.add(movieClient.save(new Movie(memberName, "Suspiria", "suspiria.jpg").type(MovieType.HORROR)))
                }
                return Single.merge(operations)
            }) as Flowable<Member>)
        }
        Flowable.merge(saves).subscribe({}, { Throwable e ->
            log.error("An error occurred saving member data: ${e.message}", e)
        })
    }

    static void main(String... args) {
        Micronaut.run(Application, args)
    }
}
