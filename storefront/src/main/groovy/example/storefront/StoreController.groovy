package example.storefront

import example.api.v1.Movie
import example.api.v1.Member
import example.storefront.client.v1.CommentClient
import example.storefront.client.v1.MovieClient
import example.storefront.client.v1.MemberClient
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.PathVariable
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.RxStreamingHttpClient
import io.micronaut.http.sse.Event

@Controller("/")
class StoreController {

    private final MemberClient memberClient
    private final MovieClient movieClient
    private final CommentClient commentClient

    StoreController(
            MemberClient memberClient,
            MovieClient movieClient,
            CommentClient commentClient) {
        
        this.memberClient = memberClient
        this.movieClient = movieClient
        this.commentClient = commentClient
    }

    @Produces(MediaType.TEXT_HTML)
    @Get(uri = '/')
    HttpResponse index() {
        HttpResponse.redirect(URI.create('/index.html'))
    }

    @Get('/movies')
    Single<List<Movie>> movies() {
        movieClient.list()
                .onErrorReturnItem(Collections.emptyList())
    }

    @Get('/tests')
    Maybe<Movie> tests() {
        movieClient.random()
    }

    @Get('/movies/random')
    Maybe<Movie> randomMovie() {
        movieClient.find('star-wars')
    }

    @Get('/movies/{slug}')
    Maybe<Movie> showMovie(@PathVariable('slug') String slug) {
        movieClient.find slug
    }

    

    @Get('/movies/member/{member}')
    Single<List<Movie>> moviesForMember(String member) {
        movieClient.byMember(member)
                .onErrorReturnItem(Collections.emptyList())
    }

    @Get('/members')
    Single<List<Member>> members() {
        memberClient.list()
                    .onErrorReturnItem(Collections.emptyList())
    }

}
