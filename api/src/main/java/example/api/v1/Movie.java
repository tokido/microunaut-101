package example.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;

public class Movie {

    private String slug;
    private String image;
    private String name;
    protected MovieType type = MovieType.SCIFI;
    private String member;

    @JsonCreator
    public Movie(@JsonProperty("member") String member, @JsonProperty("name") String name,
            @JsonProperty("image") String image) {
        this.member = member;
        this.name = name;
        this.image = image;
    }

    Movie() {
    }

    @NotBlank
    public String getMember() {
        return member;
    }

    @NotBlank
    public String getName() {
        return name;
    }

    @NotBlank
    public String getSlug() {
        if (slug != null) {
            return slug;
        }
        return member + "-" + name;
    }

    @NotBlank
    public String getImage() {
        return image;
    }

    public MovieType getType() {
        return type;
    }

    public Movie type(MovieType type) {
        if (type != null) {
            this.type = type;
        }
        return this;
    }

    public Movie slug(String slug) {
        if (slug != null) {
            this.slug = slug;
        }
        return this;
    }

    protected void setImage(String image) {
        this.image = image;
    }

    protected void setSlug(String slug) {
        this.slug = slug;
    }

    protected void setType(MovieType type) {
        this.type = type;
    }

    void setMember(String member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "Movie{" + "name='" + name + '\'' + ", type=" + type + ", member='" + member + '\'' + ", slug='" + member
                + '\'' + ", image='" + image + '\'' + '}';
    }
}
