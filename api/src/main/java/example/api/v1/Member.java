package example.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

public class Member {

    protected String name;
    protected List<Movie> movies = Collections.emptyList();

    @JsonCreator
    public Member(@JsonProperty("name") String name) {
        this.name = name;
    }

    protected Member() {
    }

    public String getName() {
        return name;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
