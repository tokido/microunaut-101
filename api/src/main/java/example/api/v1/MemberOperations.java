package example.api.v1;

import io.reactivex.Single;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import java.util.List;

/**
 * @author graemerocher
 * @since 1.0
 */
public interface MemberOperations {

    @Get("/")
    Single<List<Member>> list();

    @Get("/names")
    Single<List<String>> names();

    @Post("/")
    Single<Member> save(String name);
}
