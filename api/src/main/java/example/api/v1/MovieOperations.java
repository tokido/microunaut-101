
package example.api.v1;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.validation.Validated;

import javax.validation.Valid;
import java.util.List;

@Validated
public interface MovieOperations<T extends Movie> {

    @Get("/")
    Single<List<T>> list();

    @Get("/member/{name}")
    Single<List<T>> byMember(String name);

    @Get("/random")
    Maybe<T> random();

    @Get("/{slug}")
    Maybe<T> find(String slug);

    @Post("/")
    Single<T> save(@Valid @Body T movie);
}
