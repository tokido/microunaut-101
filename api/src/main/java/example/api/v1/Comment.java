package example.api.v1;

import java.util.Date;

public interface Comment {

    String getPoster();

    String getContent();

    Date getDateCreated();
}
